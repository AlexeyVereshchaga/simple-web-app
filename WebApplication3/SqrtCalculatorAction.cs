﻿using System;

namespace WebApplication3
{
    public class SqrtCalculatorAction : CalculatorAction
    {
        private Int32 NumberFirst { get; set; }

        public SqrtCalculatorAction(Int32 numberFirst)
        {
            NumberFirst = numberFirst;
        }
        public override Double Calculate()
        {
            return Math.Sqrt(Convert.ToDouble(NumberFirst));
        }
    }
}