﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebApplication3
{
    public abstract class Creator
    {
        public abstract CalculatorAction CreateCalculatorAction (XmlDocument xml); //FactoryMethod
    }
}