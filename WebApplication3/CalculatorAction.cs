﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3
{
    public abstract class CalculatorAction
    {
       public abstract Double Calculate();
    }
}