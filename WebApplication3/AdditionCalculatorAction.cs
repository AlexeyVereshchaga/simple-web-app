﻿using System;

namespace WebApplication3
{
    public class AdditionCalculatorAction : CalculatorAction
    {

        private Int32 NumberFirst { get; set; }
        private Int32 NumberSecond { get; set; }

        public AdditionCalculatorAction (Int32 numberFirst, Int32 numberSecond)
        {
            NumberFirst = numberFirst;
            NumberSecond = numberSecond;
        }
        public override Double Calculate()
        {
            return NumberFirst + NumberSecond;
        }
    }
}