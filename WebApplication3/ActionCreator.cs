﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace WebApplication3
{
    public class ActionCreator : Creator
    {
        public override CalculatorAction CreateCalculatorAction(XmlDocument xml)
        {
            // temporary 
            // strange behaviour function GetElementsByTagName() with the same nodes tag 
            // for example <number>2</number><number>4</number>
            String actionStr = GetTextFromFirstNode("action", xml);
            Action action = (Action)Enum.Parse(typeof(Action), actionStr, true);
            String number1 = GetTextFromFirstNode("number1", xml);
            String number2 = GetTextFromFirstNode("number2", xml);
            switch (action)
            {
                case Action.Addition:
                    return new AdditionCalculatorAction(Convert.ToInt32(number1), Convert.ToInt32(number2));
                case Action.Sqrt:
                    return new SqrtCalculatorAction(Convert.ToInt32(number1));
                default:
                    throw new ArgumentException("The action type " + action + " is not recognized.");
            }
        }

        /// <summary>
        /// Get text of first element frome list of nodes
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private String GetTextFromFirstNode(String tag, XmlDocument xmlDoc)
        {
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName(tag);
            return nodeList.Item(0).InnerText;
        }
    }
}