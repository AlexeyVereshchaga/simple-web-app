﻿using System.Text;
using System.Web;
using System.Xml;

namespace WebApplication3
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var requestBytes = context.Request.BinaryRead(context.Request.ContentLength);
            string xmlStr = Encoding.UTF8.GetString(requestBytes);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlStr);

            Creator creator = new ActionCreator();
            CalculatorAction calcAction = creator.CreateCalculatorAction(xmlDoc);

            context.Response.ContentType = "text/plain";
            context.Response.Write(calcAction.Calculate());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}